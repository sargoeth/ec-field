import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
from scipy.interpolate import griddata

path = "/media/sarah/storage/Data/Douglas Bakkum/sampledata/"
file_name = path+'footprint.mat'
map2_name = "/media/sarah/storage/Data/Douglas Bakkum/transformed.mat"
mat_contents = sio.loadmat(file_name)
map2_image = sio.loadmat(map2_name)

# Load electrode coordinates
n_elec = 206
x_coord = mat_contents['footprint'][0][0][6][0]
y_coord = mat_contents['footprint'][0][0][7][0]

# Load average traces
n_samples = 101
n_traces = mat_contents['footprint'][0][0][5][0]
tot_traces = mat_contents['footprint'][0][0][4]

# Load MAP2 image
x_start = int(map2_image['t'][0][0][1])
x_end = int(map2_image['t'][0][0][2])
y_start = int(map2_image['t'][0][0][3])
y_end = int(map2_image['t'][0][0][4])
map2_intensity = map2_image['t'][0][0][0]

# Sorting the data

cmap = plt.get_cmap('gnuplot')
colors = [cmap(i) for i in np.linspace(0, 1, n_elec)]

# Sorting in x-dir

sorted_xind = np.argsort(x_coord)

# Sorting in y-dir

sorted_yind = np.zeros((n_elec-2))

for i in range(12):
    xx = y_coord[sorted_xind][17*i:17*(i+1)]
    sort_xx = np.argsort(xx) + int(17*i)
    sorted_yind[17*i:17*(i+1)] = sort_xx

sorted_yind = sorted_yind.astype(int)

# Sort all the data

sorted_xcoord = x_coord[sorted_xind][sorted_yind]
sorted_ycoord = y_coord[sorted_xind][sorted_yind]

sorted_ntraces = n_traces[sorted_xind][sorted_yind]
sorted_tottraces = tot_traces[:,sorted_xind][:,sorted_yind]

# Average the two columns that correspond to the same electrode

sorted_xcoord = np.delete(sorted_xcoord, np.linspace(6*17-1,7*17-1,18, dtype=int))

sorted_ycoord[5*17:6*17] = sorted_ycoord[17:2*17]
sorted_ycoord = np.delete(sorted_ycoord, np.linspace(6*17-1,7*17-1,18, dtype=int))

sorted_tottraces = np.delete(sorted_tottraces, np.linspace(6*17-1,7*17-1,18, dtype=int), axis=1)

# Plot
plt.figure(2, figsize=(6,6))
plt.imshow(map2_intensity, extent=[x_start,x_end,y_end, y_start])
plt.plot(x_coord, y_coord, 'k.')
plt.scatter(sorted_xcoord, sorted_ycoord, sorted_tottraces[50,:]**2, alpha=0.5)
plt.ylim((2100, 1750))
plt.xlim((x_start,x_end))

plt.show()


