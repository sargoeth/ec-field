import scipy.io as sio
from neo.core import (Block, Segment, ChannelIndex, AnalogSignal)
import pickle
import matplotlib.pyplot as plt
from elephant.current_source_density import estimate_csd
from quantities import kHz, um
import numpy as np

path = "/media/sarah/storage/Data/Douglas Bakkum/sampledata/"
file_name = path+'footprint.mat'

mat_contents = sio.loadmat(file_name)

### Pre processing of the data

# Load electrode coordinates
n_elec = 206
x_coord = mat_contents['footprint'][0][0][6][0]
#pickle.dump(x_coord, open('x_coord.p', 'wb'))
#reader = neo.io.PickleIO(filename='x_coord.p')
#x_coord = reader.read_block()
#
y_coord = mat_contents['footprint'][0][0][7][0]
#pickle.dump(y_coord, open('y_coord.p', 'wb'))
#reader = neo.io.PickleIO(filename='y_coord.p')
#y_coord = reader.read_block()

# Load average traces
tot_traces = mat_contents['footprint'][0][0][4]
#pickle.dump(tot_traces, open('tot_traces.p', 'wb'))
#reader = neo.io.PickleIO(filename='tot_traces.p')
#tot_traces = reader.read() # 101 x 206, i.e. time x electrode

# Sorting the data
# Sorting in x-dir

sorted_xind = np.argsort(x_coord)

# Sorting in y-dir

sorted_yind = np.zeros((n_elec-2))

for i in range(12):
    xx = y_coord[sorted_xind][17*i:17*(i+1)]
    sort_xx = np.argsort(xx) + int(17*i)
    sorted_yind[17*i:17*(i+1)] = sort_xx

sorted_yind = sorted_yind.astype(int)

# Sort all the data
sorted_xcoord = x_coord[sorted_xind][sorted_yind]
sorted_ycoord = y_coord[sorted_xind][sorted_yind]
sorted_tottraces = tot_traces[:,sorted_xind][:,sorted_yind]

# Average the two columns that correspond to the same electrode
sorted_xcoord = np.delete(sorted_xcoord, np.linspace(6*17-1,7*17-1,18, dtype=int))
sorted_ycoord[5*17:6*17] = sorted_ycoord[17:2*17]
sorted_ycoord = np.delete(sorted_ycoord, np.linspace(6*17-1,7*17-1,18, dtype=int))
sorted_tottraces = np.delete(sorted_tottraces, np.linspace(6*17-1,7*17-1,18, dtype=int), axis=1)

coords = np.zeros((186,2))
for i in range(186):
    coords[i,:] = [sorted_xcoord[i], sorted_ycoord[i]]

# create NEO object
blk = Block()

# only one segment because on trial
seg = Segment(name='seg0', index=0)
blk.segments.append(seg)

# create AnalogSignals one for each electrode
ansig = AnalogSignal(signal=sorted_tottraces, units='uV', sampling_rate=20*kHz)

## add electrode coordinates
#chx = ChannelIndex(name='MEA', index=range(206), channel_ids=range(206),
#                   channel_names=['Channel %i' % chid for chid in range(8)],
#                   coordinates=coords)
#
#chx.analogsignals.append(ansig)
#ansig.channel_index = chx
#blk.segments[0].analogsignals.append(ansig)
 
# csd
#csd = estimate_csd(ansig, coords*um, method = 'KCSD2D')
csd_moi = estimate_csd(ansig, coords*um, method = 'MoIKCSD')

# neuron morpho
map2_name = "/media/sarah/storage/Data/Douglas Bakkum/transformed.mat"
map2_image = sio.loadmat(map2_name)

# Load MAP2 image
x_start = int(map2_image['t'][0][0][1])
x_end = int(map2_image['t'][0][0][2])
y_start = int(map2_image['t'][0][0][3])
y_end = int(map2_image['t'][0][0][4])
map2_intensity = map2_image['t'][0][0][0]

# Plot
y, x = np.mgrid[slice(min(sorted_ycoord), max(sorted_ycoord) , (max(sorted_ycoord)- min(sorted_ycoord))/100),
                slice(min(sorted_xcoord), max(sorted_xcoord) , (max(sorted_xcoord)- min(sorted_xcoord))/100)]

plt.figure('instantaeous', figsize=(8,6))
#plt.plot(x_coord, y_coord, 'k.')
plt.imshow(map2_intensity, extent=[x_start,x_end,y_end, y_start],  cmap=plt.cm.gray)
plt.pcolor(x, y, csd[50].T, alpha=0.5, edgecolor=(1.0, 1.0, 1.0, 0.3), linewidth=0.0015625)
plt.colorbar()
plt.ylim((2100, 1750))
plt.xlim((x_start,x_end))
plt.grid(False)


plt.show()













