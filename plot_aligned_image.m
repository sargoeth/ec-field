function plot_aligned_image(im,contr)

% plot_aligned_image(im)
% plot_aligned_image(im,1) for contrast enhancement

if nargin<2
    contr=0;
end

if iscell(im)
    
    for i=1:length(im)
        
        dx(i)=im{i}.xend-im{i}.xstart;
        
    end
    
    [v o]=sort(dx,'descend');
    
%     figure
    hold on
    for i=o
        plot_aligned_image(im{i});
    end
    axis ij
    return
    
end
        
if contr
    im.IM=imadjust(im.IM)
end

% figure
imagesc([im.xstart im.xend],[im.ystart im.yend],im.IM);
axis equal;
colormap(jet);
hold on;
% plot(ntk2.x,ntk2.y,'w.');


