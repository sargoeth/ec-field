import numpy as np
import os
import scipy.io as sio
import scipy as sp
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
import subprocess
import matplotlib as mpl

path = "/media/sarah/storage/Data/Douglas Bakkum/sampledata/"
file_name = path+'footprint.mat'
map2_name = "/media/sarah/storage/Data/Douglas Bakkum/transformed.mat"
mat_contents = sio.loadmat(file_name)['footprint'][0][0]
map2_image = sio.loadmat(map2_name)['t'][0][0]

# # Load electrode coordinates
n_elec = 206
x_coord = mat_contents[6].flatten()
y_coord = mat_contents[7].flatten()

# # Load average traces
n_samples = 101
n_traces = mat_contents[5].flatten()
tot_traces = mat_contents[4]

# # Load MAP2 image
x_start = int(map2_image[1])
x_end = int(map2_image[2])
y_start = int(map2_image[3])
y_end = int(map2_image[4])
map2_intensity = map2_image[0]


# # Plot
# plt.figure(2, figsize=(6,6))
# plt.imshow(map2_intensity, extent=[x_start,x_end,y_end, y_start])
# plt.plot(x_coord, y_coord, 'k.')
# plt.scatter(x_coord, y_coord, tot_traces[50,:]**2, alpha=0.5)
# plt.ylim((2100, 1750))
# plt.xlim((x_start,x_end))

# plt.show()



class MidpointNormalize(mpl.colors.Normalize):
    def __init__(self, vmin, vmax, midpoint=0, clip=False):
        self.midpoint = midpoint
        mpl.colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))


def make_movie(template, scatter=True, output=False):

    files = []

    amin = template.min()
    amax = template.max()

    import matplotlib.colors as colors
    norm = MidpointNormalize(vmin=amin, vmax=amax, midpoint=0)
    print (amin, amax)

    if not scatter:

        x_width = 16.2
        y_width = 9.8
        my_cmap = plt.get_cmap('seismic')
        scalarMap = plt.cm.ScalarMappable(norm=norm, cmap=my_cmap)

    for i in range(template.shape[0]):  # 50 frames
        fig, ax = plt.subplots(figsize=(5, 5))
        plt.imshow(map2_intensity, extent=[x_start,x_end,y_end, y_start], cmap=plt.cm.gray)
        #plt.plot(x_coord, y_coord, 'k.')
        if scatter is True:
            plt.scatter(x_coord, y_coord, s=np.abs(tot_traces[i]), c=tot_traces[i].flatten(), alpha=0.75, cmap=plt.cm.seismic, norm=norm)
            plt.colorbar()
            plt.clim(amin, amax)
            plt.ylim((2100, 1750))
            plt.xlim((x_start,x_end))
        else:
            import matplotlib.patches as patches
            for j in range(len(tot_traces[i])):
                color = scalarMap.to_rgba(tot_traces[i, j])
                rect = patches.Rectangle((x_coord[j]-x_width/2,y_coord[j]-x_width/2),x_width,y_width,linewidth=0,edgecolor=color,facecolor=color, alpha=0.75)
                # Add the patch to the Axes
                ax.add_patch(rect)
            #ax.colorbar()
            #ax.clim(amin, amax)
            ax.set_ylim((2100, 1750))
            ax.set_xlim((x_start,x_end))
        
        if output is not None:
            fname = '_tmp%03d.png' % i
            print('Saving frame', fname)
            plt.savefig(fname)
            files.append(fname)
        plt.close()

    if output is not None:
        print('Making movie animation.mpg - this may take a while')
        subprocess.call("mencoder 'mf://_tmp*.png' -mf type=png:fps=10 -ovc lavc "
                        "-lavcopts vcodec=wmv2 -oac copy -o %s.mpg" %output, shell=True)

        # cleanup
        for fname in files:
            os.remove(fname)
            
def make_movie_csd(template, x_coord, y_coord, output=False):

    files = []

    amin = template.min()
    amax = template.max()

#    import matplotlib.colors as colors
#    norm = MidpointNormalize(vmin=amin, vmax=amax, midpoint=0)
    print (amin, amax)
#
#    my_cmap = plt.get_cmap('seismic')
#    scalarMap = plt.cm.ScalarMappable(norm=norm, cmap=my_cmap)

    for i in range(template.shape[0]):  # 50 frames
        #fig, ax = plt.subplots(figsize=(5, 5))
        y, x = np.mgrid[slice(min(y_coord), max(y_coord) , (max(y_coord)- min(y_coord))/100),
                slice(min(x_coord), max(x_coord) , (max(x_coord)- min(x_coord))/100)]
        fig = plt.subplots(figsize=(5, 5))
        plt.imshow(map2_intensity, extent=[x_start,x_end,y_end, y_start], cmap=plt.cm.gray)
        
        plt.pcolor(x, y, template[i].T, alpha=0.5, edgecolor=(1.0, 1.0, 1.0, 0.3), linewidth=0.0015625)
        plt.colorbar()
        plt.clim(amin, amax)
        plt.ylim((2100, 1750))
        plt.xlim((x_start,x_end))
        
#        import matplotlib.patches as patches
#        for j in range(len(tot_traces[i])):
#            color = scalarMap.to_rgba(tot_traces[i, j])
#            rect = patches.Rectangle((x_coord[j]-x_width/2,y_coord[j]-x_width/2),x_width,y_width,linewidth=0,edgecolor=color,facecolor=color, alpha=0.75)
#            # Add the patch to the Axes
#            ax.add_patch(rect)
#        #ax.colorbar()
#        #ax.clim(amin, amax)
#        ax.set_ylim((2100, 1750))
#        ax.set_xlim((x_start,x_end))
        

        if output is not None:
            fname = '_tmp%03d.png' % i
            print('Saving frame', fname)
            plt.savefig(fname)
            files.append(fname)
        plt.close()

    if output is not None:
        print('Making movie animation.mpg - this may take a while')
        subprocess.call("mencoder 'mf://_tmp*.png' -mf type=png:fps=10 -ovc lavc "
                        "-lavcopts vcodec=wmv2 -oac copy -o %s.mpg" %output, shell=True)

        # cleanup
        for fname in files:
            os.remove(fname)

